## 0.1.2 - 2016-07-13

* Fix when `end_date` is nil [#7](https://github.com/treasure-data/embulk-input-google_analytics/pull/7)

## 0.1.1 - 2016-07-13
* Enable scheduled execution [#4](https://github.com/treasure-data/embulk-input-google_analytics/pull/4)
* Error handling [#6](https://github.com/treasure-data/embulk-input-google_analytics/pull/6)
* Ignore too early accessing data due to it is not fixed value [#5](https://github.com/treasure-data/embulk-input-google_analytics/pull/5)

## 0.1.0 - 2016-07-07

The first release!!
